var app = angular.module('chirpApp', ['ngRoute', 'ngResource']).run(function($http, $rootScope){
    $rootScope.authenticated = false;
    $rootScope.current_user = '';
    
    $rootScope.signout = function(){
        $http.get('auth/signout');
        $rootScope.authenticated = false;
        $rootScope.current_user = '';
    };
});

app.config(function($routeProvider){
    $routeProvider
        .when('/', {
            templateUrl: 'View/main.html',
            controller: 'mainController'
        })
        .when('/login', {
            templateUrl: 'View/login.html',
            controller: 'authController'
        })
        .when('/register', {
            templateUrl: 'View/register.html',
            controller: 'authController'
        })
        .otherwise({
            redirectTo: '/'
        });
});