app.factory('postService', ['$resource', function($resource){
    return $resource('/api/posts/:id');
}]);